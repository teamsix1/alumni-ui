# ui-alumni

## Getting started

To make it easy for you to get started with GitLab, here's a list of recommended next steps.

```
  cd ui-alumni
  npm install
  npm run dev
```

## Add your files

- [ ] [Create](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#create-a-file) or [upload](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#upload-a-file) files
- [ ] [Add files using the command line](https://docs.gitlab.com/ee/gitlab-basics/add-file.html#add-a-file-using-the-command-line) or push an existing Git repository with the following command:

```
cd existing_repo
git remote add origin https://gitlab.com/teamsix1/ui-alumni.git
git branch -M main
git push -uf origin main
```

## Test and Deploy

-Install docker

-Build your docker image

```
    docker build . -t my-app
    # Sending build context to Docker daemon  884.7kB
    # ...
    # Successfully built 4b00e5ee82ae
    # Successfully tagged my-app:latest
```

-Run your docker image

```
    docker run -d -p 8080:80 my-app
    curl localhost:8080
    # <!DOCTYPE html><html lang=en>...</html>
```

## Name

ui-alumni

## Description

project OS subject KMITL

## Installation

- [node.js](https://nodejs.org/en/)

## Usage

- [vue.js](https://vuejs.org/)
